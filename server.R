
# This is the server logic for a Shiny web application.
# You can find out more about building applications with Shiny here:
#
# http://shiny.rstudio.com
#
library(shiny)
library(ggplot2)
library(shinyjs)
library(shinydashboard)
library(ggplot2)
library(RSQLite)
library(shinyBS)
library(rintrojs)
library(DT)

library(shiny)

source('helpers.R')

shinyServer(function(input, output, session) {
  shinyjs::useShinyjs()
   reactive({
     getExpoParams(session)
   })
  
  
  observeEvent(input$submit,{
    expos <- input$expos
    exposureSidebar(session, expos)
    # for(name in expos){
    #   print(name)
    #   print("===")
    # }
  })

})
